use crate::{is_reserved_identifier, json, types::Item, Error, JsonDataChecker, Result, Validator};

const METHOD_NAME: &str = "invoke_method";

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Method {
    pub(crate) id: String,
    pub(crate) name: String,
    desc: Option<String>,
    required: bool,
    call_type: CallType,
    method: String,
    input: Vec<Item>,
    output: Vec<Item>,
}

impl Method {
    pub fn new(id: &str, name: &str) -> Self {
        Self {
            id: id.to_string(),
            name: name.to_string(),
            desc: None,
            required: false,
            call_type: CallType::Sync,
            method: METHOD_NAME.to_string(),
            input: vec![],
            output: vec![],
        }
    }

    pub fn add_input(mut self, item: Item) -> Self {
        self.input.push(item);
        self
    }

    pub fn add_output(mut self, item: Item) -> Self {
        self.output.push(item);
        self
    }
    pub fn check_input(&self, value: json::Value) -> Result<()> {
        self.input.check(value)
    }

    pub fn check_output(&self, value: json::Value) -> Result<()> {
        self.output.check(value)
    }
}

impl Validator for Method {
    fn validate(&mut self) -> Result<()> {
        if self.id.is_empty() {
            return Err(Error::InvalidTsl("Method id cannot be empty".to_string()));
        }
        if is_reserved_identifier(&self.id) {
            return Err(Error::InvalidTsl(format!(
                "Method id {} is a reserved identifier",
                self.id
            )));
        }
        if self.method.ne(&METHOD_NAME) {
            return Err(Error::InvalidTsl(format!(
                "Method method {} is not {}",
                self.method, METHOD_NAME
            )));
        }
        if self.name.is_empty() {
            return Err(Error::InvalidTsl("Method name cannot be empty".to_string()));
        }
        for data in self.input.iter_mut() {
            data.validate()?;
        }
        for data in self.output.iter_mut() {
            data.validate()?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub enum CallType {
    #[serde(rename = "sync")]
    Sync,
    #[serde(rename = "async")]
    Async,
}
