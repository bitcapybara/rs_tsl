#![allow(dead_code)]
mod errors;
mod event;
mod method;
mod property;
mod tsl;
mod types;

pub use errors::{Error, Result};
pub use event::Event;
pub use method::Method;
pub use property::{Property, PropertyBuilder};
pub(crate) use serde_json as json;
pub use tsl::Tsl;
pub use types::{DataType, Item};

pub trait Validator {
    fn validate(&mut self) -> Result<()>;
}

pub trait DataChecker {
    fn check_str(&self, value: &str) -> Result<()>;
    fn check_slice(&self, value: &[u8]) -> Result<()>;
}

pub(crate) trait JsonDataChecker {
    fn check(&self, value: json::Value) -> Result<()>;
}

pub(crate) const RESERVED_IDENTIFIERS: &[&str] =
    &["set", "get", "post", "property", "event", "time", "value"];

pub(crate) fn is_reserved_identifier(identifier: &str) -> bool {
    RESERVED_IDENTIFIERS.contains(&identifier)
}
