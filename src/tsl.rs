use std::collections::HashSet;

use crate::errors::{self, Result};
use crate::event::Event;
use crate::method::Method;
use crate::property::Property;
use crate::Validator;

use serde_json as json;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Profile {
    version: String,
    product_id: String,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Tsl {
    profile: Profile,
    properties: Vec<Property>,
    events: Vec<Event>,
    methods: Vec<Method>,
}

impl Tsl {
    pub fn new(product_id: &str) -> Self {
        Self {
            profile: Profile {
                version: "1.0".into(),
                product_id: product_id.into(),
            },
            properties: vec![],
            events: vec![],
            methods: vec![],
        }
    }
    pub fn is_empty(&self) -> bool {
        if !self.properties.is_empty() {
            return false;
        }
        if !self.events.is_empty() {
            return false;
        }
        if !self.methods.is_empty() {
            return false;
        }
        true
    }

    fn profile(&self) -> Profile {
        self.profile.clone()
    }

    fn properties(&self) -> Vec<Property> {
        self.properties.clone()
    }

    fn events(&self) -> Vec<Event> {
        self.events.clone()
    }

    fn methods(&self) -> Vec<Method> {
        self.methods.clone()
    }

    pub fn add_property(&mut self, mut property: Property) -> Result<()> {
        property.validate()?;
        self.properties.push(property);
        Ok(())
    }

    pub fn add_event(&mut self, mut event: Event) -> Result<()> {
        event.validate()?;
        self.events.push(event);
        Ok(())
    }

    pub fn add_method(&mut self, mut method: Method) -> Result<()> {
        method.validate()?;
        self.methods.push(method);
        Ok(())
    }

    fn del_properties<S, I>(&mut self, ids: I) -> Result<()>
    where
        S: Into<String>,
        I: IntoIterator<Item = S>,
    {
        let ids = ids
            .into_iter()
            .map(|id| id.into())
            .collect::<HashSet<String>>();
        self.properties = self
            .properties()
            .into_iter()
            .filter(|p| !ids.contains(&p.id))
            .collect::<Vec<_>>();
        Ok(())
    }

    fn del_events<S, I>(&mut self, _ids: I) -> Result<()>
    where
        S: Into<String>,
        I: IntoIterator<Item = S>,
    {
        let ids = _ids
            .into_iter()
            .map(|id| id.into())
            .collect::<HashSet<String>>();
        self.events = self
            .events()
            .into_iter()
            .filter(|e| !ids.contains(&e.id))
            .collect::<Vec<_>>();
        Ok(())
    }

    fn del_methods<S, I>(&mut self, _ids: I) -> Result<()>
    where
        S: Into<String>,
        I: IntoIterator<Item = S>,
    {
        let ids = _ids
            .into_iter()
            .map(|id| id.into())
            .collect::<HashSet<String>>();
        self.methods = self
            .methods()
            .into_iter()
            .filter(|m| !ids.contains(&m.id))
            .collect::<Vec<_>>();
        Ok(())
    }
}

impl std::convert::TryFrom<&str> for Tsl {
    type Error = errors::Error;

    fn try_from(s: &str) -> Result<Self> {
        let mut tsl = json::from_str::<Tsl>(s)?;
        tsl.validate()?;
        Ok(tsl)
    }
}

impl std::convert::TryFrom<&[u8]> for Tsl {
    type Error = errors::Error;

    fn try_from(v: &[u8]) -> Result<Self> {
        let mut tsl = json::from_slice::<Tsl>(v)?;
        tsl.validate()?;
        Ok(tsl)
    }
}

impl std::convert::TryFrom<String> for Tsl {
    type Error = errors::Error;

    fn try_from(v: String) -> Result<Self> {
        let mut tsl = json::from_str::<Tsl>(&v)?;
        tsl.validate()?;
        Ok(tsl)
    }
}

impl std::convert::TryFrom<Vec<u8>> for Tsl {
    type Error = errors::Error;

    fn try_from(v: Vec<u8>) -> Result<Self> {
        let mut tsl = json::from_slice::<Tsl>(&v)?;
        tsl.validate()?;
        Ok(tsl)
    }
}

impl Validator for Tsl {
    fn validate(&mut self) -> Result<()> {
        for prop in self.properties.iter_mut() {
            prop.validate()?
        }
        Ok(())
    }
}
