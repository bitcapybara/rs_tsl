use super::{json, DataTypeEnum, Error, JsonDataChecker, NumberType, Result, Validator};

const INT_MAX_VALUE: i64 = 2147483647;
const INT_MIN_VALUE: i64 = -2147483648;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct IntType(NumberType);

impl IntType {
    pub(super) fn new(nt: NumberType) -> Self {
        Self(nt)
    }
    fn get_type(&self) -> DataTypeEnum {
        DataTypeEnum::Int
    }

    pub(crate) fn skip_serialize(&self) -> bool {
        self.clone().validate().is_err()
    }
}

impl Validator for IntType {
    fn validate(&mut self) -> Result<()> {
        self.0.validate::<i64>(INT_MIN_VALUE, INT_MAX_VALUE)
    }
}

impl JsonDataChecker for IntType {
    fn check(&self, value: json::Value) -> Result<()> {
        if let json::Value::Number(n) = value {
            if n.is_f64() {
                return Err(Error::InvalidData(format!("{} is not an integer", n)));
            }

            if !n.is_i64() {
                return Err(Error::InvalidData(format!("{} is not an integer", n)));
            }

            let num = n.as_i64().unwrap();
            let (min, max) = self.0.get_range::<i64>().unwrap();
            if !(min..=max).contains(&num) {
                return Err(Error::InvalidData(format!(
                    "{} is not in range {}..{}",
                    n, min, max
                )));
            }

            Ok(())
        } else {
            Err(Error::InvalidData(format!("{} is not a number", value)))
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_int_type_validate() {
        let mut tt = IntType(NumberType {
            min: "-2".into(),
            max: "5".into(),
            unit: "".into(),
            unit_name: "".into(),
            step: "2".into(),
        });

        assert!(tt.validate().is_ok());

        tt.0.min = "6".into();
        assert!(tt.validate().is_err());

        tt.0.min = "-2".into();
        tt.0.step = "8".into();
        assert!(tt.validate().is_err());

        tt.0.step = "-1".into();
        assert!(tt.validate().is_ok());
    }

    #[test]
    fn test_int_type_check() {
        let tt = IntType(NumberType {
            min: "-2".into(),
            max: "5".into(),
            unit: "".into(),
            unit_name: "".into(),
            step: "2".into(),
        });

        assert!(tt.check(json::json!(-3)).is_err());
        assert!(tt.check(json::json!(6)).is_err());
        assert!(tt.check(json::json!(3)).is_ok());

        assert!(tt.check(json::json!(2.5)).is_err());
    }
}
