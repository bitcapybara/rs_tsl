use super::{json, DataType, DataTypeEnum, Error, JsonDataChecker, Result, Validator};

const ARRAY_DEFAULT_LENGTH: u16 = 128;
const ARRAY_MAX_LENGTH: u16 = 2048;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub(super) struct ArrayType {
    size: u32,
    item: Box<DataType>,
}

impl ArrayType {
    pub(super) fn new(size: u32, item: DataType) -> Self {
        Self {
            size,
            item: Box::new(item),
        }
    }
    fn get_type(&self) -> DataTypeEnum {
        DataTypeEnum::Array
    }

    pub(crate) fn skip_serialize(&self) -> bool {
        self.clone().validate().is_err()
    }
}

impl Validator for ArrayType {
    fn validate(&mut self) -> Result<()> {
        if self.size > ARRAY_MAX_LENGTH as u32 {
            return Err(Error::InvalidTsl("array size must be less than 512".into()));
        }
        if self.size == 0 {
            self.size = ARRAY_DEFAULT_LENGTH as u32;
        }
        let item = self.item.type_e.to_owned();
        match item {
            DataTypeEnum::Struct
            | DataTypeEnum::Int
            | DataTypeEnum::Float
            | DataTypeEnum::Double
            | DataTypeEnum::Text => self.item.validate(),
            _ => Err(Error::InvalidTsl(format!(
                "Array type item can only be a struct, int, float, double or text, got {}",
                item
            ))),
        }
    }
}

impl JsonDataChecker for ArrayType {
    fn check(&self, value: json::Value) -> Result<()> {
        if let json::Value::Array(arr) = value {
            if arr.len() > self.size as usize {
                return Err(Error::InvalidData(format!(
                    "array size must be less than {}",
                    self.size
                )));
            }
            for v in arr {
                self.item.check(v)?;
            }
            return Ok(());
        }
        Err(Error::InvalidTsl("Array type must be an array".into()))
    }
}
