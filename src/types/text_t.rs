use super::{json, DataTypeEnum, Error, JsonDataChecker, Result, Validator};

const TEXT_MAX_LENGTH: u16 = 2048;
const TEXT_MIN_LENGTH: u16 = 1;

#[derive(Debug, Clone, Copy, serde::Serialize, serde::Deserialize)]
pub struct TextType {
    length: u16,
}

impl TextType {
    pub fn new(length: u16) -> Self {
        Self { length }
    }
    fn get_type(&self) -> DataTypeEnum {
        DataTypeEnum::Text
    }

    pub(crate) fn skip_serialize(&self) -> bool {
        self.clone().validate().is_err()
    }
}

impl Validator for TextType {
    fn validate(&mut self) -> Result<()> {
        let len = self.length;
        if !(TEXT_MIN_LENGTH..=TEXT_MAX_LENGTH).contains(&len) {
            return Err(Error::InvalidTsl(format!(
                "Text type length must be between 1 and {}",
                TEXT_MAX_LENGTH
            )));
        }
        Ok(())
    }
}

impl JsonDataChecker for TextType {
    fn check(&self, value: json::Value) -> Result<()> {
        if let json::Value::String(s) = value {
            if s.is_empty() || s.len() > self.length as usize {
                return Err(Error::InvalidData(format!(
                    "Text type length must be between 1 and {}",
                    TEXT_MAX_LENGTH
                )));
            }
            Ok(())
        } else {
            Err(Error::InvalidTsl("Text type must be a string".to_string()))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_text_type_validate() {
        let mut tt = TextType { length: 20 };
        assert!(tt.validate().is_ok());

        tt.length = 2049;
        assert!(tt.validate().is_err());

        tt.length = 0;
        assert!(tt.validate().is_err());
    }

    #[test]
    fn test_text_type_check() {
        let tt = TextType { length: 20 };
        let mut s = String::new();
        assert!(tt.check(json::json!(s)).is_err());

        (0..=19).for_each(|_| s += "a");
        assert!(tt.check(json::json!(s)).is_ok());

        s += "a";
        assert!(tt.check(json::json!(s)).is_err());

        assert!(tt.check(json::json!(0)).is_err());
    }
}
