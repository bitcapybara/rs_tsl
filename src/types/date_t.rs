use super::{json, DataTypeEnum, Error, JsonDataChecker, Result, Validator};

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct DateType;

impl DateType {
    pub fn new() -> Self {
        Self
    }
    fn get_type(&self) -> DataTypeEnum {
        DataTypeEnum::Date
    }

    pub(crate) fn skip_serialize(&self) -> bool {
        true
    }
}

impl Validator for DateType {
    fn validate(&mut self) -> Result<()> {
        Ok(())
    }
}

impl JsonDataChecker for DateType {
    fn check(&self, value: json::Value) -> Result<()> {
        if let json::Value::String(s) = value {
            if let Ok(date) = s.parse::<i64>() {
                if date < 0 {
                    return Err(Error::InvalidData(format!("{} is not a valid date", s)));
                }
                return Ok(());
            }
            return Err(Error::InvalidData(format!("{} can not parse to an int", s)));
        }
        Err(Error::InvalidData(format!("{} is not a string", value)))
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_date_type_check() {
        let tt = DateType;

        assert!(tt.check(json::json!(3)).is_err());
        assert!(tt.check(json::json!("-1")).is_err());
        assert!(tt.check(json::json!("1")).is_ok());
    }
}
