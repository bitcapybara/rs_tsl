use super::{json, DataTypeEnum, Error, JsonDataChecker, NumberType, Result, Validator};

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct DoubleType(NumberType);

impl DoubleType {
    pub(super) fn new(nt: NumberType) -> Self {
        Self(nt)
    }
    fn get_type(&self) -> DataTypeEnum {
        DataTypeEnum::Double
    }

    pub(crate) fn skip_serialize(&self) -> bool {
        self.clone().validate().is_err()
    }
}

impl Validator for DoubleType {
    fn validate(&mut self) -> Result<()> {
        self.0.validate::<f64>(f64::MIN, f64::MAX)
    }
}

impl JsonDataChecker for DoubleType {
    fn check(&self, value: json::Value) -> Result<()> {
        if let json::Value::Number(n) = value {
            if !n.is_f64() {
                return Err(Error::InvalidData(format!("{} is not a float", n)));
            }
            let (min, max) = self.0.get_range::<f64>().unwrap();
            if n.as_f64().unwrap() < min || n.as_f64().unwrap() > max {
                return Err(Error::InvalidData(format!(
                    "{} is not in range {}..{}",
                    n, min, max
                )));
            }
            Ok(())
        } else {
            Err(Error::InvalidData(format!("{} is not a number", value)))
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_double_type_check() {
        let tt = DoubleType(NumberType {
            min: "-2.0".into(),
            max: "5.0".into(),
            unit: "".into(),
            unit_name: "".into(),
            step: "1.1".into(),
        });

        assert!(tt.check(json::json!(-3.1)).is_err());
        assert!(tt.check(json::json!(5.0001)).is_err());
        assert!(tt.check(json::json!(3.0)).is_ok());

        assert!(tt.check(json::json!(2)).is_err());
    }
}
