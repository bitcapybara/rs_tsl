use super::{json, DataTypeEnum, Error, JsonDataChecker, Result, Validator};

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct BoolType {
    #[serde(rename = "0")]
    false_v: String,
    #[serde(rename = "1")]
    true_v: String,
}

impl BoolType {
    pub(super) fn new(false_v: &str, true_v: &str) -> Self {
        Self {
            false_v: false_v.into(),
            true_v: true_v.into(),
        }
    }
    fn get_type(&self) -> DataTypeEnum {
        DataTypeEnum::Bool
    }

    pub(crate) fn skip_serialize(&self) -> bool {
        self.clone().validate().is_err()
    }
}

impl Validator for BoolType {
    fn validate(&mut self) -> Result<()> {
        if self.false_v.is_empty() || self.true_v.is_empty() {
            return Err(Error::InvalidTsl(
                "Bool type must have both true and false values".to_string(),
            ));
        }
        Ok(())
    }
}

impl JsonDataChecker for BoolType {
    fn check(&self, value: json::Value) -> Result<()> {
        if let json::Value::Number(n) = value {
            if !n.is_i64() {
                return Err(Error::InvalidData(format!("{} is not an int", n)));
            }
            let n = n.as_i64().unwrap();
            if n != 0 && n != 1 {
                return Err(Error::InvalidData(format!("{} is not 0 or 1", n)));
            }
            Ok(())
        } else {
            Err(Error::InvalidData(format!("{} is not a number", value)))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bool_type_validate() {
        let mut bt = BoolType {
            false_v: "关闭".to_string(),
            true_v: "开启".to_string(),
        };
        assert!(bt.validate().is_ok());

        bt.false_v = "".to_string();
        assert!(bt.validate().is_err());

        bt.false_v = "关闭".to_string();
        bt.true_v = "".to_string();
        assert!(bt.validate().is_err());
    }

    #[test]
    fn test_bool_type_check() {
        let bt = BoolType {
            false_v: "关闭".to_string(),
            true_v: "开启".to_string(),
        };
        assert!(bt.check(json::json!(0)).is_ok());
        assert!(bt.check(json::json!(1)).is_ok());
        assert!(bt.check(json::json!(2)).is_err());
        assert!(bt.check(json::json!("0")).is_err());
    }
}
