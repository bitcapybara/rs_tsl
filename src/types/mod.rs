mod array_t;
mod bool_t;
mod date_t;
mod double_t;
mod enum_t;
mod float_t;
mod int_t;
mod struct_t;
mod text_t;

use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
    ops::Sub,
    str::FromStr,
};

use crate::{errors::Error, errors::Result, json, JsonDataChecker, Validator};

use self::{
    array_t::ArrayType, bool_t::BoolType, date_t::DateType, double_t::DoubleType, enum_t::EnumType,
    float_t::FloatType, int_t::IntType, struct_t::StructType, text_t::TextType,
};

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct DataType {
    #[serde(rename = "type")]
    type_e: DataTypeEnum,
    #[serde(skip_serializing_if = "TypeSpec::skip_serialize")]
    spec: TypeSpec,
}

impl DataType {
    fn new(spec: TypeSpec) -> Result<Self> {
        let mut t = Self {
            type_e: spec.type_e(),
            spec,
        };
        t.validate()?;
        Ok(t)
    }

    pub fn new_array(size: u32, item: DataType) -> Result<Self> {
        Self::new(TypeSpec::Array(ArrayType::new(size, item)))
    }

    pub fn new_bool(false_v: &str, true_v: &str) -> Result<Self> {
        Self::new(TypeSpec::Bool(BoolType::new(false_v, true_v)))
    }

    pub fn new_date() -> Result<Self> {
        Self::new(TypeSpec::Date(DateType::new()))
    }

    pub fn new_number(min: &str, max: &str, step: &str) -> NumberTypeBuilder {
        NumberTypeBuilder {
            min: min.to_string(),
            max: max.to_string(),
            unit: "".to_string(),
            unit_name: "".to_string(),
            step: step.to_string(),
        }
    }

    pub fn new_enum(values: HashMap<i64, String>) -> Result<Self> {
        Self::new(TypeSpec::Enum(EnumType::new(values)))
    }

    pub fn new_struct(items: Vec<Item>) -> Result<Self> {
        Self::new(TypeSpec::Struct(StructType::new(items)))
    }

    pub fn new_text(size: u16) -> Result<Self> {
        Self::new(TypeSpec::Text(TextType::new(size)))
    }
}

impl Validator for DataType {
    fn validate(&mut self) -> Result<()> {
        self.spec.validate()
    }
}

impl JsonDataChecker for DataType {
    fn check(&self, value: serde_json::Value) -> Result<()> {
        self.spec.check(value)
    }
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
#[serde(untagged)]
enum TypeSpec {
    Array(ArrayType),
    Bool(BoolType),
    Date(DateType),
    Double(DoubleType),
    Float(FloatType),
    Int(IntType),
    Text(TextType),
    Struct(StructType),
    Enum(EnumType),
}

impl TypeSpec {
    fn skip_serialize(&self) -> bool {
        match self {
            TypeSpec::Array(array_type) => array_type.skip_serialize(),
            TypeSpec::Bool(bool_type) => bool_type.skip_serialize(),
            TypeSpec::Date(date_type) => date_type.skip_serialize(),
            TypeSpec::Double(double_type) => double_type.skip_serialize(),
            TypeSpec::Float(float_type) => float_type.skip_serialize(),
            TypeSpec::Int(int_type) => int_type.skip_serialize(),
            TypeSpec::Text(text_type) => text_type.skip_serialize(),
            TypeSpec::Struct(struct_type) => struct_type.skip_serialize(),
            TypeSpec::Enum(enum_type) => enum_type.skip_serialize(),
        }
    }

    fn type_e(&self) -> DataTypeEnum {
        match &self {
            TypeSpec::Array(_) => DataTypeEnum::Array,
            TypeSpec::Bool(_) => DataTypeEnum::Bool,
            TypeSpec::Date(_) => DataTypeEnum::Date,
            TypeSpec::Double(_) => DataTypeEnum::Double,
            TypeSpec::Float(_) => DataTypeEnum::Float,
            TypeSpec::Int(_) => DataTypeEnum::Int,
            TypeSpec::Text(_) => DataTypeEnum::Text,
            TypeSpec::Struct(_) => DataTypeEnum::Struct,
            TypeSpec::Enum(_) => DataTypeEnum::Enum,
        }
    }
}

impl Validator for TypeSpec {
    fn validate(&mut self) -> Result<()> {
        match self {
            TypeSpec::Array(array_type) => array_type.validate(),
            TypeSpec::Bool(bool_type) => bool_type.validate(),
            TypeSpec::Date(date_type) => date_type.validate(),
            TypeSpec::Double(double_type) => double_type.validate(),
            TypeSpec::Float(float_type) => float_type.validate(),
            TypeSpec::Int(int_type) => int_type.validate(),
            TypeSpec::Text(text_type) => text_type.validate(),
            TypeSpec::Struct(struct_type) => struct_type.validate(),
            TypeSpec::Enum(enum_type) => enum_type.validate(),
        }
    }
}

impl JsonDataChecker for TypeSpec {
    fn check(&self, value: json::Value) -> Result<()> {
        match self {
            TypeSpec::Array(array_type) => array_type.check(value),
            TypeSpec::Bool(bool_type) => bool_type.check(value),
            TypeSpec::Date(date_type) => date_type.check(value),
            TypeSpec::Double(double_type) => double_type.check(value),
            TypeSpec::Float(float_type) => float_type.check(value),
            TypeSpec::Int(int_type) => int_type.check(value),
            TypeSpec::Text(text_type) => text_type.check(value),
            TypeSpec::Struct(struct_type) => struct_type.check(value),
            TypeSpec::Enum(enum_type) => enum_type.check(value),
        }
    }
}

#[derive(Debug, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
pub enum DataTypeEnum {
    #[serde(rename = "int")]
    Int,
    #[serde(rename = "float")]
    Float,
    #[serde(rename = "double")]
    Double,
    #[serde(rename = "text")]
    Text,
    #[serde(rename = "date")]
    Date,
    #[serde(rename = "bool")]
    Bool,
    #[serde(rename = "enum")]
    Enum,
    #[serde(rename = "struct")]
    Struct,
    #[serde(rename = "array")]
    Array,
}

impl std::convert::From<DataTypeEnum> for String {
    fn from(data_type_enum: DataTypeEnum) -> Self {
        match data_type_enum {
            DataTypeEnum::Int => "int".to_string(),
            DataTypeEnum::Float => "float".to_string(),
            DataTypeEnum::Double => "double".to_string(),
            DataTypeEnum::Text => "text".to_string(),
            DataTypeEnum::Date => "date".to_string(),
            DataTypeEnum::Bool => "bool".to_string(),
            DataTypeEnum::Enum => "enum".to_string(),
            DataTypeEnum::Struct => "struct".to_string(),
            DataTypeEnum::Array => "array".to_string(),
        }
    }
}

impl std::fmt::Display for DataTypeEnum {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            DataTypeEnum::Int => write!(f, "int"),
            DataTypeEnum::Float => write!(f, "float"),
            DataTypeEnum::Double => write!(f, "double"),
            DataTypeEnum::Text => write!(f, "text"),
            DataTypeEnum::Date => write!(f, "date"),
            DataTypeEnum::Bool => write!(f, "bool"),
            DataTypeEnum::Enum => write!(f, "enum"),
            DataTypeEnum::Struct => write!(f, "struct"),
            DataTypeEnum::Array => write!(f, "array"),
        }
    }
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub(super) struct NumberType {
    min: String,
    max: String,
    unit: String,
    unit_name: String,
    step: String,
}

impl NumberType {
    fn get_range<T: FromStr>(&self) -> Result<(T, T)> {
        let min = self.min.parse::<T>().map_err(|_| Error::Internal)?;
        let max = self.max.parse::<T>().map_err(|_| Error::Internal)?;
        Ok((min, max))
    }

    fn validate<T>(&mut self, min_value: T, max_value: T) -> Result<()>
    where
        T: FromStr + PartialOrd + Copy + Display + Sub<Output = T>,
    {
        if let Ok((min, max)) = self.get_range::<T>() {
            if max < min {
                return Err(Error::InvalidTsl(
                    "number type min value must be less than max value".to_string(),
                ));
            }
            if !(min_value..=max_value).contains(&min) {
                return Err(Error::InvalidTsl(format!(
                    "number type min value must be between {} and {}",
                    min_value, max_value
                )));
            }
            if !(min_value..=max_value).contains(&max) {
                return Err(Error::InvalidTsl(format!(
                    "number type max value must be between {} and {}",
                    min_value, max_value
                )));
            }
            if let Ok(step) = self.step.parse::<T>() {
                if step > max - min {
                    return Err(Error::InvalidTsl(format!(
                        "number type step value must be less than {}",
                        max - min
                    )));
                }
                return Ok(());
            }
            return Err(Error::InvalidTsl(
                "number type step cannot parse to valid type".to_string(),
            ));
        }
        return Err(Error::InvalidTsl(format!(
            "Int type range must be between {} and {}",
            std::i64::MIN,
            std::i64::MAX
        )));
    }
}

pub struct NumberTypeBuilder {
    min: String,
    max: String,
    unit: String,
    unit_name: String,
    step: String,
}

impl NumberTypeBuilder {
    pub fn new(min: &str, max: &str, step: &str) -> Self {
        Self {
            min: min.to_string(),
            max: max.to_string(),
            unit: "".to_string(),
            unit_name: "".to_string(),
            step: step.to_string(),
        }
    }

    pub fn unit(mut self, unit: String) -> Self {
        self.unit = unit;
        self
    }

    pub fn unit_name(mut self, unit_name: String) -> Self {
        self.unit_name = unit_name;
        self
    }

    pub fn build_double(self) -> Result<DataType> {
        DataType::new(TypeSpec::Double(DoubleType::new(NumberType {
            min: self.min,
            max: self.max,
            unit: self.unit,
            unit_name: self.unit_name,
            step: self.step,
        })))
    }

    pub fn build_float(self) -> Result<DataType> {
        DataType::new(TypeSpec::Float(FloatType::new(NumberType {
            min: self.min,
            max: self.max,
            unit: self.unit,
            unit_name: self.unit_name,
            step: self.step,
        })))
    }

    pub fn build_int(self) -> Result<DataType> {
        DataType::new(TypeSpec::Int(IntType::new(NumberType {
            min: self.min,
            max: self.max,
            unit: self.unit,
            unit_name: self.unit_name,
            step: self.step,
        })))
    }
}

const ITEM_ID_MAX_LENGTH: u16 = 30;
const ITEM_NAME_MAX_LENGTH: u16 = 30;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Item {
    pub(crate) id: String,
    name: String,
    data_type: DataType,
}

impl Item {
    pub fn new(id: &str, name: &str, data_type: DataType) -> Self {
        Self {
            id: id.to_string(),
            name: name.to_string(),
            data_type,
        }
    }
}

impl Validator for Item {
    fn validate(&mut self) -> Result<()> {
        if self.id.len() > ITEM_ID_MAX_LENGTH as usize {
            return Err(Error::InvalidTsl(format!(
                "Field id must be less than {} characters",
                ITEM_ID_MAX_LENGTH
            )));
        }
        if self.name.len() > ITEM_NAME_MAX_LENGTH as usize {
            return Err(Error::InvalidTsl(format!(
                "Field name must be less than {} characters",
                ITEM_NAME_MAX_LENGTH
            )));
        }
        self.data_type.validate()
    }
}

impl JsonDataChecker for Item {
    fn check(&self, value: json::Value) -> Result<()> {
        self.data_type.check(value)
    }
}

impl JsonDataChecker for Vec<Item> {
    fn check(&self, value: json::Value) -> Result<()> {
        if let json::Value::Object(ref m) = value {
            let mut all = m.keys().cloned().collect::<HashSet<_>>();
            for item in self {
                if let Some(v) = m.get(&item.id).cloned() {
                    item.check(v)?;
                    all.remove(&item.id);
                    continue;
                }
                return Err(Error::InvalidData(format!(
                    "{} is missing item {}",
                    value, item.id
                )));
            }
            if !all.is_empty() {
                return Err(Error::InvalidData(format!(
                    "{} has extra items {}",
                    value,
                    all.iter().cloned().collect::<Vec<_>>().join(", ")
                )));
            }
        }
        Err(Error::InvalidData(format!("{} is not an object", value)))
    }
}
