use std::collections::{HashMap, HashSet};

use super::{json, DataTypeEnum, Error, Item, JsonDataChecker, Result, Validator};

const STRUCT_MAX_FIELDS: u16 = 128;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct StructType(Vec<Item>);

impl StructType {
    pub fn new(items: Vec<Item>) -> Self {
        Self(items)
    }

    pub fn add_field(mut self, item: Item) -> Result<Self> {
        if self.0.len() >= STRUCT_MAX_FIELDS as usize {
            return Err(Error::InvalidTsl(format!(
                "Struct type can have at most {} fields",
                STRUCT_MAX_FIELDS
            )));
        }
        self.0.push(item);
        Ok(self)
    }
    fn get_type(&self) -> DataTypeEnum {
        DataTypeEnum::Struct
    }

    pub(crate) fn skip_serialize(&self) -> bool {
        self.clone().validate().is_err()
    }
}

impl Validator for StructType {
    fn validate(&mut self) -> Result<()> {
        if self.0.len() > STRUCT_MAX_FIELDS as usize {
            return Err(Error::InvalidTsl(format!(
                "Struct type can have at most {} fields",
                STRUCT_MAX_FIELDS
            )));
        }
        let mut field_ids = HashSet::new();
        for field in &mut self.0 {
            match field.data_type.type_e {
                DataTypeEnum::Int
                | DataTypeEnum::Float
                | DataTypeEnum::Double
                | DataTypeEnum::Bool
                | DataTypeEnum::Enum
                | DataTypeEnum::Date
                | DataTypeEnum::Text => {}
                _ => {
                    return Err(Error::InvalidTsl(format!(
                        "Struct type field {} can only have types Int, Float, Double, Bool, Enum, Date or Text",
                        field.name
                    )));
                }
            }
            field.validate()?;
            if field_ids.contains(&field.id) {
                return Err(Error::InvalidTsl(format!(
                    "Struct type field id {} is not unique",
                    field.id
                )));
            }
            field_ids.insert(field.id.clone());
        }
        Ok(())
    }
}

impl JsonDataChecker for StructType {
    fn check(&self, value: json::Value) -> Result<()> {
        if let json::Value::Object(ref o) = value {
            let fields = self.0.clone();
            let mut entries = fields
                .iter()
                .map(|f| (f.id.clone(), f))
                .collect::<HashMap<_, _>>();
            for field in &self.0 {
                if let Some(v) = o.get(&field.name).cloned() {
                    field.check(v)?;
                } else {
                    return Err(Error::InvalidData(format!(
                        "{} is missing field {}",
                        value, field.name
                    )));
                }
                entries.remove(&field.id);
            }
            if !entries.is_empty() {
                return Err(Error::InvalidData(format!(
                    "{} has extra fields {}",
                    value,
                    entries.keys().cloned().collect::<Vec<_>>().join(", ")
                )));
            }
            Ok(())
        } else {
            Err(Error::InvalidData(format!("{} is not an object", value)))
        }
    }
}
