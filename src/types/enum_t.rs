use std::collections::HashMap;

use super::{json, DataTypeEnum, Error, JsonDataChecker, Result, Validator};

const ENUM_MIN_KEY: i64 = i32::MIN as i64;
const ENUM_MAX_KEY: i64 = i32::MAX as i64;
const ENUM_MAX_VALUE_LENTH: usize = 32;
const ENUM_MAXX_NUM: u16 = 128;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct EnumType(HashMap<i64, String>);

impl EnumType {
    pub fn new(values: HashMap<i64, String>) -> Self {
        Self(values)
    }
    fn get_type(&self) -> DataTypeEnum {
        DataTypeEnum::Enum
    }

    pub(crate) fn skip_serialize(&self) -> bool {
        self.clone().validate().is_err()
    }
}

impl Validator for EnumType {
    fn validate(&mut self) -> Result<()> {
        if self.0.len() > ENUM_MAXX_NUM as usize {
            return Err(Error::InvalidTsl(format!(
                "Enum type can have at most {} values",
                ENUM_MAXX_NUM
            )));
        }
        for (k, v) in self.0.iter() {
            if !(ENUM_MIN_KEY..=ENUM_MAX_KEY).contains(k) {
                return Err(Error::InvalidTsl(format!(
                    "Enum type value must be between {} and {}",
                    ENUM_MIN_KEY, ENUM_MAX_KEY
                )));
            }
            if v.len() > ENUM_MAX_VALUE_LENTH {
                return Err(Error::InvalidTsl(format!(
                    "Enum type value length must be less than {}",
                    ENUM_MAX_VALUE_LENTH
                )));
            }
        }
        Ok(())
    }
}

impl JsonDataChecker for EnumType {
    fn check(&self, value: json::Value) -> Result<()> {
        if let json::Value::Number(n) = value {
            if !n.is_i64() {
                return Err(Error::InvalidData(format!(
                    "{} is not a positive integer",
                    n
                )));
            }
            let n = n.as_i64().unwrap();
            if n < ENUM_MIN_KEY || n > ENUM_MAX_KEY {
                return Err(Error::InvalidData(format!(
                    "{} is not in range {}..{}",
                    n, ENUM_MIN_KEY, ENUM_MAX_KEY
                )));
            }
            if !self.0.contains_key(&n) {
                return Err(Error::InvalidData(format!("{} is not a valid value", n)));
            }
            Ok(())
        } else {
            Err(Error::InvalidData(format!("{} is not a number", value)))
        }
    }
}
