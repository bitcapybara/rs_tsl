pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("{op} {msg} err")]
    OpJson { op: String, msg: String },
    #[error("invalid json literal: {0}")]
    RawJson(#[from] serde_json::Error),
    #[error("invalid tsl: {0}")]
    InvalidTsl(String),
    #[error("invalid data: {0}")]
    InvalidData(String),
    #[error("internal error")]
    Internal,
}

impl Error {
    pub(crate) fn invalid_json<S: Into<String>>(op: Op, msg: S) -> Self {
        Error::OpJson {
            op: op.to_string(),
            msg: msg.into(),
        }
    }
}

#[derive(Debug)]
pub enum Op {
    Serialize,
    Deserialize,
}

impl std::fmt::Display for Op {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Op::Serialize => write!(f, "serialize"),
            Op::Deserialize => write!(f, "deserialize"),
        }
    }
}
