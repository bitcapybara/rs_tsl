use crate::{
    is_reserved_identifier, json, types::DataType, DataChecker, Error, JsonDataChecker, Result,
    Validator,
};

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Property {
    pub(crate) id: String,
    pub(crate) name: String,
    desc: Option<String>,
    access_mode: AccessMode,
    required: bool,
    data_type: DataType,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub enum AccessMode {
    #[serde(rename = "r")]
    Read,
    #[serde(rename = "rw")]
    ReadWrite,
}

// 物模型校验
impl Validator for Property {
    fn validate(&mut self) -> Result<()> {
        if self.id.is_empty() {
            return Err(Error::InvalidTsl("Property id cannot be empty".to_string()));
        }
        if is_reserved_identifier(&self.id) {
            return Err(Error::InvalidTsl(format!(
                "Property id {} is a reserved identifier",
                self.id
            )));
        }
        if self.name.is_empty() {
            return Err(Error::InvalidTsl(
                "Property name cannot be empty".to_string(),
            ));
        }
        self.data_type.validate()?;
        Ok(())
    }
}

// 物模型数据校验
impl DataChecker for Property {
    fn check_str(&self, value: &str) -> Result<()> {
        self.data_type.check(json::from_str::<json::Value>(value)?)
    }

    fn check_slice(&self, value: &[u8]) -> Result<()> {
        self.data_type
            .check(json::from_slice::<json::Value>(value)?)
    }
}

pub struct PropertyBuilder {
    id: String,
    name: String,
    desc: Option<String>,
    access_mode: AccessMode,
    required: bool,
    data_type: DataType,
}

impl PropertyBuilder {
    pub fn new(id: &str, name: &str, data_type: DataType) -> Self {
        PropertyBuilder {
            id: id.into(),
            name: name.into(),
            desc: None,
            access_mode: AccessMode::Read,
            required: false,
            data_type,
        }
    }

    pub fn desc(mut self, desc: String) -> Self {
        self.desc = Some(desc);
        self
    }

    pub fn access_mode(mut self, access_mode: AccessMode) -> Self {
        self.access_mode = access_mode;
        self
    }

    pub fn required(mut self, required: bool) -> Self {
        self.required = required;
        self
    }

    pub fn build(self) -> Property {
        Property {
            id: self.id,
            name: self.name,
            desc: self.desc,
            access_mode: self.access_mode,
            required: self.required,
            data_type: self.data_type,
        }
    }
}
