use crate::{
    is_reserved_identifier, json, types::Item, DataChecker, Error, JsonDataChecker, Result,
    Validator,
};

const EVENT_METHOD_NAME: &str = "event_report";

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Event {
    pub(crate) id: String,
    pub(crate) name: String,
    desc: Option<String>,
    #[serde(rename = "type")]
    e_type: EventType,
    required: bool,
    method: String,
    output: Vec<Item>,
}

impl Event {
    pub fn new(id: &str, name: &str) -> Self {
        Self {
            id: id.to_string(),
            name: name.to_string(),
            desc: None,
            e_type: EventType::Info,
            required: false,
            method: EVENT_METHOD_NAME.to_string(),
            output: vec![],
        }
    }

    pub fn add_output(mut self, item: Item) -> Self {
        self.output.push(item);
        self
    }
}

impl Validator for Event {
    fn validate(&mut self) -> Result<()> {
        if self.id.is_empty() {
            return Err(Error::InvalidTsl("Event id cannot be empty".to_string()));
        }
        if is_reserved_identifier(&self.id) {
            return Err(Error::InvalidTsl(format!(
                "Event id {} is a reserved identifier",
                self.id
            )));
        }
        if self.method.ne(&EVENT_METHOD_NAME) {
            return Err(Error::InvalidTsl(format!(
                "Event method {} is not {}",
                self.method, EVENT_METHOD_NAME
            )));
        }
        for data in self.output.iter_mut() {
            data.validate()?;
        }
        Ok(())
    }
}

impl DataChecker for Event {
    fn check_str(&self, value: &str) -> Result<()> {
        self.check(json::from_str::<json::Value>(value)?)
    }

    fn check_slice(&self, value: &[u8]) -> Result<()> {
        self.check(json::from_slice::<json::Value>(value)?)
    }
}

impl JsonDataChecker for Event {
    fn check(&self, value: json::Value) -> Result<()> {
        self.output.check(value)
    }
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub enum EventType {
    #[serde(rename = "info")]
    Info,
    #[serde(rename = "alert")]
    Alert,
    #[serde(rename = "error")]
    Error,
}
