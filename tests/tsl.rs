use std::collections::HashMap;

use rs_tsl::{DataType, Event, Item, Method, PropertyBuilder, Tsl};

#[test]
fn test_tsl_create() {
    let mut tsl = Tsl::new("test");

    let d_text = DataType::new_text(1).unwrap();
    let d_array = DataType::new_array(10, d_text.clone()).unwrap();
    let d_bool = DataType::new_bool("关闭", "开启").unwrap();
    let d_date = DataType::new_date().unwrap();
    let d_double = DataType::new_number("1.0", "10.0", "0.3")
        .build_double()
        .unwrap();
    let d_float = DataType::new_number("1.0", "10.0", "0.3")
        .build_float()
        .unwrap();
    let d_int = DataType::new_number("1", "10", "1").build_int().unwrap();
    let d_enum = DataType::new_enum(HashMap::from_iter(vec![
        (1, "1".to_string()),
        (2, "2".to_string()),
        (3, "3".to_string()),
    ]))
    .unwrap();
    let d_struct = DataType::new_struct(vec![
        Item::new("f_int", "f_int", d_int.clone()),
        Item::new("f_text", "f_text", d_text.clone()),
    ])
    .unwrap();

    assert!(tsl
        .add_property(PropertyBuilder::new("p_text", "p_text", d_text.clone()).build())
        .is_ok());
    assert!(tsl
        .add_property(PropertyBuilder::new("p_array", "p_array", d_array.clone()).build())
        .is_ok());
    assert!(tsl
        .add_property(PropertyBuilder::new("p_bool", "p_bool", d_bool.clone()).build())
        .is_ok());
    assert!(tsl
        .add_property(PropertyBuilder::new("p_date", "p_date", d_date.clone()).build())
        .is_ok());
    assert!(tsl
        .add_property(PropertyBuilder::new("p_double", "p_double", d_double.clone()).build())
        .is_ok());
    assert!(tsl
        .add_property(PropertyBuilder::new("p_float", "p_float", d_float.clone()).build())
        .is_ok());
    assert!(tsl
        .add_property(PropertyBuilder::new("p_int", "p_int", d_int.clone()).build())
        .is_ok());
    assert!(tsl
        .add_property(PropertyBuilder::new("p_enum", "p_enum", d_enum.clone()).build())
        .is_ok());
    assert!(tsl
        .add_property(PropertyBuilder::new("p_struct", "p_struct", d_struct.clone()).build())
        .is_ok());

    let event = Event::new("e", "e")
        .add_output(Item::new("e_text", "e_text", d_text.clone()))
        .add_output(Item::new("e_array", "e_array", d_array.clone()))
        .add_output(Item::new("e_bool", "e_bool", d_bool.clone()))
        .add_output(Item::new("e_date", "e_date", d_date.clone()))
        .add_output(Item::new("e_double", "e_double", d_double.clone()))
        .add_output(Item::new("e_float", "e_float", d_float.clone()))
        .add_output(Item::new("e_int", "e_int", d_int.clone()))
        .add_output(Item::new("e_enum", "e_enum", d_enum.clone()))
        .add_output(Item::new("e_struct", "e_struct", d_struct.clone()));
    assert!(tsl.add_event(event).is_ok());

    let method = Method::new("m", "m")
        .add_input(Item::new("m_text", "m_text", d_text))
        .add_input(Item::new("m_array", "m_array", d_array))
        .add_input(Item::new("m_bool", "m_bool", d_bool))
        .add_input(Item::new("m_date", "m_date", d_date))
        .add_input(Item::new("m_double", "m_double", d_double))
        .add_output(Item::new("m_float", "m_float", d_float))
        .add_output(Item::new("m_int", "m_int", d_int))
        .add_output(Item::new("m_enum", "m_enum", d_enum))
        .add_output(Item::new("m_struct", "m_struct", d_struct));
    assert!(tsl.add_method(method).is_ok());

    let json = serde_json::to_string(&tsl).unwrap();
    println!("{}", json);
}
